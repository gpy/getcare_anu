"""
WSGI config for website project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

# UNCOMMENT THE FOLLOWING TWO LINES IN ORDER TO RUN PROJECT ON AWS, COMMENT 
# THEM OUT WITH A STARTING '#' TO REMOVE THEM
########################################################################################
sys.path.append('/opt/bitnami/projects/getcare_anu/Django/project/getcare/website')
os.environ.setdefault("PYTHON_EGG_CACHE", "/opt/bitnami/projects/getcare/egg_cache") 
########################################################################################
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')

application = get_wsgi_application()

