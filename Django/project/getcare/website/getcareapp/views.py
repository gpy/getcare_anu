from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from django.utils import timezone
from django.urls import reverse
import random
import string
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import FileSystemStorage
from datetime import datetime, timedelta, date
import calendar as libCalendar
import json
from django.core import serializers

def getCarerbyKey(currentKey):
    try:
        return Carer.objects.get(key=currentKey)
    except ObjectDoesNotExist:
        return False


def getUserbyKey(currentKey):
    try:
        return User.objects.get(key=currentKey)
    except ObjectDoesNotExist:
        return False


def getProviderbyKey(currentKey):
    try:
        return Provider.objects.get(key=currentKey)
    except ObjectDoesNotExist:
        return False


def findingUser(currentKey):
    current_user = getUserbyKey(currentKey)
    userType = "USER"
    if (current_user == False):
        current_user = getCarerbyKey(currentKey)
        userType = "CARER"
    if (current_user == False):
        current_user = getProviderbyKey(currentKey)
        userType = "PROVIDER"
    if (current_user == False):
        userType = "NONE"
    return current_user, userType


def addAssociatedCarers(currentKey, carers):
    currentUser, userType = findingUser(currentKey)
    for carer in carers.iterator():
        print(carer.relation_code)
        addRelation(currentKey, "carer", carer.relation_code)


def findSeniors(currentKey):  # As provider or carer
    currentUser, userType = findingUser(currentKey)
    if userType == "PROVIDER":
        seniors = ProviderSeniorRelationship.objects.filter(provider=currentUser)
        return seniors
    if userType == "CARER":
        seniors = CarerSeniorRelationship.objects.filter(carer=currentUser)
        return seniors


def findCarers(currentKey):
    currentUser, userType = findingUser(currentKey)
    carers = ProviderCarerRelationship.objects.filter(provider=currentUser)
    return carers


def findProviders(currentKey):
    currentUser, userType = findingUser(currentKey)
    if userType == "USER":
        providers = ProviderSeniorRelationship.objects.filter(senior=currentUser)
        return providers
    if userType == "CARER":
        providers = ProviderCarerRelationship.objects.filter(carer=currentUser)
        return providers


def findCarersToAdd(currentKey, seniorRelationCode):  # only call this when usertype is provider
    currentUser, userType = findingUser(currentKey)
    addedSenior = User.objects.filter(relation_code=seniorRelationCode).first()
    carers = Carer.objects.none()
    carers = CarerSeniorRelationship.objects.filter(senior=addedSenior)
    carersFromCarerTable = Carer.objects.none()
    for carer in carers.iterator():
        carersFromCarerTable |= Carer.objects.filter(id=carer.id)
    return carersFromCarerTable


def findUserTypeByEmail(currentEmail):
    current_user = getUserbyEmail(currentEmail)
    userType = "USER"
    if (current_user == False):
        current_user = getCarerbyEmail(currentEmail)
        userType = "CARER"
    if (current_user == False):
        current_user = getProviderbyEmail(currentEmail)
        userType = "PROVIDER"
    if (current_user == False):
        userType = "NONE"
    return userType


def findingTasks(currentUser, userType):
    user_tasks = ""
    if userType == "USER":
        user_tasks = Tasks.objects.filter(user=currentUser)
    elif userType == "CARER":
        user_tasks = Tasks.objects.filter(carer=currentUser)
    elif userType == "PROVIDER":
        user_tasks = Tasks.objects.filter(provider=currentUser)
    return user_tasks

def findingTodos(currentUser, userType):
    user_todos = ""
    if userType == "USER":
        user_todos = Todos.objects.filter(user=currentUser)
    elif userType == "CARER":
        user_todos = Todos.objects.filter(carer=currentUser)
    elif userType == "PROVIDER":
        user_todos = Todos.objects.filter(provider=currentUser)
    return user_todos

def findingNotes(currentTask):  # finds all notes associated with a given task
    print(Notes.objects.filter(task=currentTask))
    return Notes.objects.filter(task=currentTask)


def addNote(currentKey, comment, task_id):
    currentTask = Tasks.objects.filter(id=task_id).first()
    currentUser, userType = findingUser(currentKey)
    if userType == "USER":
        Notes.objects.create(detail=comment, create_date=timezone.now(), user=currentUser, task=currentTask)
    if userType == "PROVIDER":
        Notes.objects.create(detail=comment, create_date=timezone.now(), provider=currentUser, task=currentTask)
    if userType == "CARER":
        Notes.objects.create(detail=comment, create_date=timezone.now(), carer=currentUser, task=currentTask)


def changeNote(currentKey, comment, task_id):
    currentTask = Tasks.objects.filter(id=task_id).first()
    currentUser, userType = findingUser(currentKey)
    notes = Notes.objects.filter(task=currentTask)
    if userType == "USER":
        note = notes.filter(senior=currentUser).delete()
        Notes.objects.create(detail=comment, create_date=timezone.now(), user=currentUser, task=currentTask)
    if userType == "PROVIDER":
        note = notes.filter(provider=currentUser).delete()
        Notes.objects.create(detail=comment, create_date=timezone.now(), provider=currentUser, task=currentTask)
    if userType == "CARER":
        notes.filter(carer=currentUser).delete()
        Notes.objects.create(detail=comment, create_date=timezone.now(), carer=currentUser, task=currentTask)


def findingRelations(currentUser, userType):
    relations = [None] * 2
    if userType == "USER":
        relations[0] = ProviderSeniorRelationship.objects.filter(senior=currentUser)
        relations[1] = CarerSeniorRelationship.objects.filter(senior=currentUser)
    elif userType == "CARER":
        relations[0] = ProviderCarerRelationship.objects.filter(carer=currentUser)
        relations[1] = CarerSeniorRelationship.objects.filter(carer=currentUser)
    elif userType == "PROVIDER":
        relations[0] = ProviderCarerRelationship.objects.filter(provider=currentUser)
        relations[1] = ProviderSeniorRelationship.objects.filter(provider=currentUser)
    return relations


def getUserbyEmail(currentEmail):
    try:
        return User.objects.get(email=currentEmail)
    except ObjectDoesNotExist:
        return False


def getCarerbyEmail(currentEmail):
    try:
        return Carer.objects.get(email=currentEmail)
    except ObjectDoesNotExist:
        return False


def getProviderbyEmail(currentEmail):
    try:
        return Provider.objects.get(email=currentEmail)
    except ObjectDoesNotExist:
        return False


def getTask(taskID, userKey):
    current_user, userType = findingUser(userKey)

    if userType == "USER":
        user_tasks = Tasks.objects.filter(user=current_user)
    elif userType == "CARER":
        user_tasks = Tasks.objects.filter(carer=current_user)
    elif userType == "PROVIDER":
        user_tasks = Tasks.objects.filter(provider=current_user)

    for user_task in user_tasks:
        if user_task.id == taskID:
            return Tasks.objects.get(id=taskID)
    return False


# def getNote(noteID, userKey):
#    current_user, userType = findingUser(userKey)

#    if userType == "USER":
#        user_notes = Notes.objects.filter(user=current_user)
#    elif userType == "CARER":
#        user_notes = Notes.objects.filter(carer=current_user)
#    elif userType == "PROVIDER":
#        user_notes = Notes.objects.filter(provider=current_user)

#    for user_note in user_notes:
#        if user_note.id == noteID:
#            return Notes.objects.get(id=noteID)
#    return False


def register(request):
    if request.method == "GET":
        return render(request, 'GetCare/register.html')

    # random the key
    seed = string.ascii_letters + string.digits
    key = random.sample(seed, 62)
    all_user = User.objects.all()
    i = 0
    key_list = []
    for i in range(len(all_user)):
        key_list.append(all_user[i].key)
    while key in key_list:
        key = random.sample(seed, 62)

    current_user = getUserbyEmail(request.POST['email'])
    if current_user:
        return HttpResponse(r"The email has been registered as a senior account")
    current_user = getCarerbyEmail(request.POST['email'])
    if current_user:
        return HttpResponse(r"The email has been registered as a carer account")
    current_user = getProviderbyEmail(request.POST['email'])
    if current_user:
        return HttpResponse(r"The email has been registered as a provider account")

    if request.POST['role'] == 'senior':
        if request.POST['relationCode'] == '':
            return HttpResponse("Please enter your relation code")
        try:
            mycarer = Carer.objects.get(relation_code=request.POST['relationCode'])
        except ObjectDoesNotExist:
            return HttpResponse("We cannot find this relation code in our system")

        if mycarer.User:
            return HttpResponse("The relation code has been registered")

        User.objects.create(firstName=request.POST['FirstName'],
                            lastName=request.POST['LastName'],
                            email=request.POST['email'],
                            password=request.POST['password'],
                            gender=request.POST['gender'],
                            phone=request.POST['phone'],
                            key="".join(key),
                            carer=mycarer,
                            )
        mycarer.User = User.objects.get(email=request.POST['email'])
        mycarer.save()
        return HttpResponse(r"Successful.<br><a href=\getcare\login> Login </a>")
    elif request.POST['role'] == 'carer':
        relationCode = random.randint(0, 999999)
        Carer.objects.create(firstName=request.POST['FirstName'],
                             lastName=request.POST['LastName'],
                             email=request.POST['email'],
                             password=request.POST['password'],
                             gender=request.POST['gender'],
                             phone=request.POST['phone'],
                             key="".join(key),
                             relation_code=relationCode, )
        return HttpResponse("Successful. Your relation code for senior is " + str(
            relationCode) + r"<br><a href=\getcare\login> Login </a>")
    elif request.POST['role'] == 'provider':
        relationCode = random.randint(0, 999999)
        Provider.objects.create(firstName=request.POST['FirstName'],
                                lastName=request.POST['LastName'],
                                email=request.POST['email'],
                                jobType=request.POST['jobType'],
                                password=request.POST['password'],
                                gender=request.POST['gender'],
                                phone=request.POST['phone'],
                                key="".join(key),
                                relation_code=relationCode, )
        return HttpResponse("Successful. Your relation code for senior is " + str(
            relationCode) + r"<br><a href=\getcare\login> Login </a>")


def calendar(request, currentKey, classType):
    current_user, userType = findingUser(currentKey)
    if (current_user == False):
        return HttpResponse("Invalid key" + r"<br><a href=\getcare\login> Login </a>")

    if request.POST and ('act' in request.POST) and request.POST['act'] == 'save':
        id = request.POST['id']
        tasks1 = Tasks.objects.filter(id=id).first()
        tasks1.title = request.POST['title']
        tasks1.classTask = request.POST['classTask']
        tasks1.detail = request.POST['detail']
        tasks1.start_time = request.POST['start_time']
        tasks1.save()
        return JsonResponse({'code': 1})

    if request.POST and ('act' in request.POST) and request.POST['act'] == 'delete':
        id = request.POST['id']
        tasks1 = Tasks.objects.filter(id=id).first()
        tasks1.delete()
        return JsonResponse({'code': 1})
    if request.POST and ('act' in request.POST) and request.POST['act'] == 'add':
        Tasks.objects.create(
            title=request.POST['title'],
            classTask=request.POST['classTask'],
            detail=request.POST['detail'],
            start_time=request.POST['start_time'],
            user=current_user,
            create_date=timezone.now(),
            start_date=request.POST['start_date'],
        )
        return JsonResponse({'code': 1})


    tasks1 = findingTasks(current_user, userType)  # Tasks.objects.filter(user=current_user)
    tasks = []


    if request.POST and not ('act' in request.POST) and len(request.POST) >1:
        checkbox_values = []
        for x in request.POST:
            checkbox_values.append(request.POST[x])

        for t in tasks1:
            if t.classTask in checkbox_values:
                tasks.append(
                    {'id': t.id, 'title': t.title, 'classTask': t.classTask, 'detail': t.detail,
                     'start_time': str(t.start_time), "start": str(t.start_date)})
        tasks = json.dumps(tasks)
        if userType == 'USER':
            return render(request, 'GetCare/calendar.html', {'user': current_user, 'tasks': tasks, 'checkbox': checkbox_values})
        elif userType == 'CARER':
            return render(request, 'GetCare/calendar_carer.html', {'user': current_user, 'tasks': tasks, 'checkbox': checkbox_values})
        elif userType == 'PROVIDER':
            return render(request, 'GetCare/calendar_provider.html', {'user': current_user, 'tasks': tasks, 'checkbox': checkbox_values})

    for t in tasks1:
        tasks.append(
            {'id': t.id, 'title': t.title, 'classTask': t.classTask, 'detail': t.detail,
             'start_time': str(t.start_time), "start": str(t.start_date)})
    tasks = json.dumps(tasks)
    if userType == 'USER':
        return render(request, 'GetCare/calendar.html', {'user': current_user, 'tasks': tasks})
    elif userType == 'CARER':
        return render(request, 'GetCare/calendar_carer.html', {'user': current_user, 'tasks': tasks})
    elif userType == 'PROVIDER':
        return render(request, 'GetCare/calendar_provider.html', {'user': current_user, 'tasks': tasks})


def index(request, currentKey, classTask):
    # Finding current user by Key
    current_user, userType = findingUser(currentKey)
    if (current_user == False):
        return HttpResponse("Invalid key" + r"<br><a href=\getcare\login> Login </a>")
    # Loading current user Tasks
    User_tasks = findingTasks(current_user, userType)

    # Loading current user Notes.
    # User_notes = findingNotes(current_user, userType)

    # Filting tasks which should be display
    result_tasks = []
    if classTask == "ALL":
        for i in User_tasks:
            result_tasks.append(i)
    elif classTask == "INCOMPLETED":
        for i in User_tasks:
            if i.status == 0:
                result_tasks.append(i)
    elif classTask == "COMPLETED":
        for i in User_tasks:
            if i.status == 1:
                result_tasks.append(i)
    else:
        for i in User_tasks:
            if i.classTask == classTask and i.status == 0:
                result_tasks.append(i)

    # result_notes = []
    # for i in User_notes:
    #    result_notes.append(i)

    if userType == "USER":
        return render(request, 'GetCare/index.html', {'user': current_user, 'Tasks': result_tasks})
    elif userType == "CARER":
        return render(request, 'GetCare/index_carer.html', {'user': current_user, 'Tasks': result_tasks})
    elif userType == "PROVIDER":
        return render(request, 'GetCare/index_provider.html', {'user': current_user, 'Tasks': result_tasks})
    else:
        return render(request, 'GetCare/index.html', {'user': current_user, 'Tasks': result_tasks})

    if userType == "USER":
        return render(request, 'GetCare/index.html', {'user': current_user, 'Notes': result_notes})
    elif userType == "CARER":
        return render(request, 'GetCare/index_carer.html', {'user': current_user, 'Notes': result_notes})
    elif userType == "PROVIDER":
        return render(request, 'GetCare/index_provider.html', {'user': current_user, 'Notes': result_notes})
    else:
        return render(request, 'GetCare/index.html', {'user': current_user, 'Notes': result_notes})


def login(request):
    if request.method == "GET":
        return render(request, 'GetCare/login.html')

    currentEmail = request.POST.get('email')
    typePassword = request.POST.get('password')
    userType = findUserTypeByEmail(currentEmail)

    if userType == 'USER':
        current_user = getUserbyEmail(currentEmail)
        if current_user:
            if current_user.password == typePassword:
                # return HttpResponseRedirect("/getcare/" + current_user.key + "/index/INCOMPLETED")
                return HttpResponseRedirect("/getcare/" + current_user.key + "/calendar/week")
    elif userType == 'CARER':
        current_user = getCarerbyEmail(currentEmail)
        if current_user:
            if current_user.password == typePassword:
                # return HttpResponseRedirect("/getcare/" + current_user.key + "/index/INCOMPLETED")
                return HttpResponseRedirect("/getcare/" + current_user.key + "/calendar/week")
    elif userType == 'PROVIDER':
        current_user = getProviderbyEmail(currentEmail)
        if current_user:
            if current_user.password == typePassword:
                # return HttpResponseRedirect("/getcare/" + current_user.key + "/index/INCOMPLETED")
                return HttpResponseRedirect("/getcare/" + current_user.key + "/calendar/week")

    return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")


def viewRelations(request, currentKey):
    current_user, userType = findingUser(currentKey)
    Relationships = findingRelations(current_user, userType)
    if request.method == "GET":
        if userType == "USER":
            return render(request, 'GetCare/viewrelation.html',
                          {'user': current_user, 'Relationships1': Relationships[0],
                           'Relationships2': Relationships[1]})
        elif userType == "CARER":
            return render(request, 'GetCare/viewrelation_carer.html',
                          {'user': current_user, 'Relationships1': Relationships[0],
                           'Relationships2': Relationships[1]})
        elif userType == "PROVIDER":
            return render(request, 'GetCare/viewrelation_provider.html',
                          {'user': current_user, 'Relationships1': Relationships[0],
                           'Relationships2': Relationships[1]})
    addOrRemove = request.POST.get('addorremove')
    addedUserType = request.POST.get('usertype')
    relationCode = request.POST.get('relationcode')
    if addOrRemove == "add":
        ret = addRelation(currentKey, addedUserType, relationCode)
        return ret
    elif addOrRemove == "remove":
        ret = removeRelation(currentKey, addedUserType, relationCode)
        return ret
    return render(request, 'GetCare/viewrelation.html', {'user': current_user})

def relationExist(relationType, user1, user2):
    # [user1][user2]
    if relationType == "CarerSenior":
        if len(CarerSeniorRelationship.objects.filter(carer=user1, senior=user2)) != 0:
            return True
    elif relationType == "ProviderSenior":
        if len(ProviderSeniorRelationship.objects.filter(provider=user1, senior=user2)) != 0:
            return True
    elif relationType == "ProviderCarer":
        if len(ProviderCarerRelationship.objects.filter(provider=user1, carer=user2)) != 0:
            return True
    return False

def addRelation(currentKey, addedUserType, relationCode):
    addedUserType = addedUserType.upper()
    currentUser, userType = findingUser(currentKey)
    try:
        if userType == "USER":
            if addedUserType == "CARER":
                addedCarer = Carer.objects.filter(relation_code=relationCode).first()
                if relationExist("CarerSenior", addedCarer, currentUser):
                    return HttpResponse(r"Relation already exists!")
                CarerSeniorRelationship.objects.create(carer=addedCarer,senior=currentUser, create_date=timezone.now())
                return HttpResponse(r"Successfully added new Carer")
            if addedUserType == "PROVIDER":
                addedProvider = Provider.objects.filter(relation_code=relationCode).first()
                if relationExist("ProviderSenior", addedProvider, currentUser):
                    return HttpResponse(r"Relation already exists!")
                ProviderSeniorRelationship.objects.create(provider=addedProvider, senior=currentUser,create_date=timezone.now())
                return HttpResponse(r"Successfully added new Provider")
        if userType == "CARER":
            if addedUserType == "USER":
                addedSenior = User.objects.filter(relation_code=relationCode).first()
                if relationExist("CarerSenior", currentUser, addedSenior):
                    return HttpResponse(r"Relation already exists!")
                CarerSeniorRelationship.objects.create(carer=currentUser,senior=addedSenior,create_date=timezone.now())
                return HttpResponse(r"Successfully added new Senior")
            if addedUserType == "PROVIDER":
                addedProvider = Provider.objects.filter(relation_code=relationCode).first()
                if relationExist("ProviderCarer", addedProvider, currentUser):
                    return HttpResponse(r"Relation already exists!")
                ProviderCarerRelationship.objects.create(provider=addedProvider, carer=currentUser,create_date=timezone.now())
                return HttpResponse(r"Successfully added new Provider")
        if userType == "PROVIDER":
            if addedUserType == "USER":
                addedSenior = User.objects.filter(relation_code=relationCode).first()
                if relationExist("ProviderSenior", currentUser, addedSenior):
                    return HttpResponse(r"Relation already exists!")
                ProviderSeniorRelationship.objects.create(provider=currentUser, senior=addedSenior, create_date=timezone.now())
                carers = findCarersToAdd(currentKey, relationCode)  # will return queryset of associated carers
                print(carers)
                addAssociatedCarers(currentKey, carers)
                return HttpResponse(r"Successfully added new Senior")
            if addedUserType == "CARER":
                addedCarer = Carer.objects.filter(relation_code=relationCode).first()
                if relationExist("ProviderCarer", currentUser, addedCarer):
                    return HttpResponse(r"Relation already exists!")
                ProviderCarerRelationship.objects.create(provider=currentUser,carer=addedCarer,create_date=timezone.now())
                return HttpResponse(r"Successfully added new Carer")
    except:
        return HttpResponse(r"Your Relation Code is incorrect!")
    # return HttpResponse(r"Your Relation Code is incorrect!")


def removeRelation(currentKey, removedUserType, relationCode):
    removedUserType = removedUserType.upper()
    currentUser, userType = findingUser(currentKey)
    try:
        if userType == "USER":
            if removedUserType == "CARER":
                relationToDelete = CarerSeniorRelationship.objects.filter(
                    carer=Carer.objects.filter(relation_code=relationCode).first()).first()
                if relationToDelete != None:
                    relationToDelete.delete()
                    return HttpResponse(r"Successfully removed Carer")
                else:
                    return HttpResponse(r"No provider by that code found!")
            if removedUserType == "PROVIDER":
                relationToDelete = ProviderSeniorRelationship.objects.filter(
                    provider=Provider.objects.filter(relation_code=relationCode).first()).first()
                if relationToDelete != None:
                    relationToDelete.delete()
                    return HttpResponse(r"Successfully removed Provider")
                else:
                    return HttpResponse(r"No provider by that code found!")
        if userType == "CARER":
            if removedUserType == "USER":
                relationToDelete = CarerSeniorRelationship.objects.filter(
                    senior=User.objects.filter(relation_code=relationCode).first()).first()
                if relationToDelete != None:
                    relationToDelete.delete()
                else:
                    return HttpResponse(r"No senior by that code found!")
                return HttpResponse(r"Successfully removed Senior")
            if removedUserType == "PROVIDER":
                relationToDelete = ProviderCarerRelationship.objects.filter(
                    provider=Provider.objects.filter(relation_code=relationCode).first()).first()
                if relationToDelete != None:
                    relationToDelete.delete()
                else:
                    return HttpResponse(r"No provider by that code found!")
                return HttpResponse(r"Successfully removed Provider")
        if userType == "PROVIDER":
            if removedUserType == "USER":
                relationToDelete = ProviderSeniorRelationship.objects.filter(
                    senior=User.objects.filter(relation_code=relationCode).first()).first()
                if relationToDelete != None:
                    relationToDelete.delete()
                else:
                    return HttpResponse(r"No Senior by that code found!")
                return HttpResponse(r"Successfully removed Senior")
            if removedUserType == "CARER":
                relationToDelete = ProviderCarerRelationship.objects.filter(
                    carer=Carer.objects.filter(relation_code=relationCode).first()).first()
                if relationToDelete != None:
                    relationToDelete.delete()
                else:
                    return HttpResponse(r"No Carer by that code found!")
                return HttpResponse(r"Successfully removed Carer")
    except:
        return HttpResponse(r"Your Relation Code is incorrect!")
    # return HttpResponse(r"Your Relation Code is incorrect!")


def uploadCSV(request, currentKey):
    # current_user, userType = findingUser(currentKey)
    # return render(request,'GetCare/csvisuploaded.html',{'user':current_user})
    csv_file = request.FILES["newTaskCSV"]
    tasksCSV = csv_file.read().decode("utf-8")
    tasks = tasksCSV.split("\n")
    counter = 0
    current_user, userType = findingUser(currentKey)
    if userType == "CARER":
        for task in tasks:
            counter = counter + 1
            if not task:
                break
            try:
                values = task.split(",")
                title = values[0]
                date = values[1]
                startTime = values[2]
                endTime = values[3]
                detail = values[4]
                myProvider = values[5]
                mySenior = values[6]
                Tasks.objects.create(title=title, create_date=timezone.now(), detail=detail, start_date=date, start_time=startTime, end_time=endTime,
                                   carer=current_user, provider=myProvider, senior=mySenior)
            except:
                return HttpResponse(r"CSV Upload failed: ERROR IN LINE " + counter)

    if userType == "USER":
        for task in tasks:
            counter = counter + 1
            if not task:
                break
            try:
                values = task.split(",")
                title = values[0]
                date = values[1]
                startTime = values[2]
                endTime = values[3]
                detail = values[4]
                myProvider = values[5]
                myCarer = values[6]
                Tasks.objects.create(title=title, create_date=timezone.now(), detail=detail, start_date=date, start_time=startTime, end_time=endTime,
                                 user=current_user, provider=myProvider, carer=myCarer)
            except:
                return HttpResponse(r"CSV Upload failed: ERROR IN LINE " + counter)

    if userType == "PROVIDER":
        for task in tasks:
            counter = counter + 1               
            if not task:
                break
            try:
                values = task.split(",")
                title = values[0]
                date = values[1]
                time = values[2]
                detail = values[3]
                senior = values[4]
                carer = values[5]
                Tasks.objects.create(title=title, create_date=timezone.now(), detail=detail, start_date=date, start_time=startTime, end_time=endTime,
                                 provider=current_user, senior=senior, carer = carer)
            except:
                return HttpResponse(r"CSV Upload failed: ERROR IN LINE " + counter)

    return render(request, 'GetCare/csvisuploaded.html', {'user': current_user})


def ProviderUploadCSV(request, currentKey):  # currently unused -- needs work
    # current_user, userType = findingUser(currentKey)
    # return render(request,'GetCare/csvisuploaded.html',{'user':current_user})

    csv_file = request.FILES["newTaskCSV"]
    tasksCSV = csv_file.read().decode("utf-8")
    current_user, userType = findingUser(currentKey)
    tasks = tasksCSV.split("\n")
    for task in tasks:
        if not task:
            break
        print(task)
        values = task.split(",")
        title = values[0]
        taskType = values[1]
        date = values[2]
        detail = values[3]
        myProvider = None
        if (userType == "USER"):
            Tasks.objects.create(title=title, create_date=timezone.now(), detail=detail, start_date=date,
                                 user=current_user, classTask=taskType, provider=myProvider, carer=current_user.carer)
        elif (userType == "CARER"):
            Tasks.objects.create(title=title, create_date=timezone.now(), detail=detail, start_date=date,
                                 carer=current_user, classTask=taskType)
        elif (userType == "PROVIDER"):
            Tasks.objects.create(title=title, create_date=timezone.now(), detail=detail, start_date=date,
                                 provider=current_user, classTask=taskType)
    return render(request, 'GetCare/csvisuploaded.html', {'user': current_user})


def newTask(request, currentKey):
    current_user, userType = findingUser(currentKey)

    templates = Template.objects.filter(ownerEmail=current_user.email, provider__isnull=False)
    # filter out "new task for me/carer"
    jstemplates = serializers.serialize("json", templates)

    if request.method == "POST" and 'title' not in request.POST:

        provider = int(request.POST["classTask"])

        if userType == "USER":
            relations = findProviders(currentKey)
            # relationsTwo = findSeniors(currentKey)
            # print(relationsTwo)
            return render(request, 'GetCare/newtask.html',
                          {'user': current_user, 'relations': relations, 'templates': templates, 'jstemplates': jstemplates, 'provider':provider})
        elif userType == "CARER":

            relations = findProviders(currentKey)
            relationsTwo = findSeniors(currentKey)
            print(relationsTwo)
            return render(request, 'GetCare/newtask_carer.html',
                          {'user': current_user, 'relations': relations, 'relationsTwo': relationsTwo, 'templates': templates, 'jstemplates': jstemplates, 'provider':provider })
        elif userType == "PROVIDER":
            relations = findSeniors(currentKey)
            return render(request, 'GetCare/newtask_provider.html',
                          {'user': current_user, 'relations': relations, 'templates': templates, 'jstemplates': jstemplates,'provider':provider})
        else:
            return render(request, 'GetCare/newtask.html',
                          {'user': current_user, 'relations': relations, 'templates': templates, 'jstemplates': jstemplates,'provider':provider})


    if request.method == "GET":
        if userType == "USER":
            relations = findProviders(currentKey)
            # relationsTwo = findSeniors(currentKey)
            # print(relationsTwo)
            return render(request, 'GetCare/newtask.html',
                          {'user': current_user, 'relations': relations, 'templates': templates, 'jstemplates': jstemplates})
        elif userType == "CARER":

            relations = findProviders(currentKey)
            relationsTwo = findSeniors(currentKey)
            print(relationsTwo)
            return render(request, 'GetCare/newtask_carer.html',
                          {'user': current_user, 'relations': relations, 'relationsTwo': relationsTwo, 'templates': templates, 'jstemplates': jstemplates})
        elif userType == "PROVIDER":
            relations = findSeniors(currentKey)
            return render(request, 'GetCare/newtask_provider.html', {'user': current_user, 'relations': relations, 'templates': templates, 'jstemplates': jstemplates})
        else:
            return render(request, 'GetCare/newtask.html', {'user': current_user, 'relations': relations, 'templates': templates, 'jstemplates': jstemplates})


    currTitle = request.POST.get('title')
    CurrDate = request.POST.get('date')
    CurrDetail = request.POST.get('detail')
    CurrTime = request.POST.get('time')
    Frequency = request.POST.get('task-frequency')

    print('time', CurrTime)

    # CurrComment = request.POST.get('comments')
    # CurrTime = request.POST.get('time')
    # myProvider = None

    dateList = []
    
    if Frequency == "Once":
        dateList.append(CurrDate)
    else: # Week or Month
        EndDate = request.POST.get('end-date')
        Starts = datetime.strptime(CurrDate, '%Y-%m-%d')
        Ends = datetime.strptime(EndDate, '%Y-%m-%d')

        if CurrDate >= EndDate:
            return HttpResponse(r"Task End Date should be later than Task Start Date!")

    if Frequency == "Week":
        newDate = Starts
        while newDate <= Ends:
            dateList.append(newDate.strftime('%Y-%m-%d'))
            newDate = newDate + timedelta(weeks=1)
    elif Frequency == "Month":
        day = Starts.day
        month = Starts.month
        year = Starts.year
        newDate = Starts
        while newDate <= Ends:
            print(newDate)
            dateList.append(newDate.strftime('%Y-%m-%d'))
            if month != 12:
                month += 1
            else:
                year += 1
                month = 1
            if libCalendar.monthrange(year, month)[1] >= day:
                newDate = datetime.strptime("-".join([str(year), str(month), str(day)]), '%Y-%m-%d')
            else:
                newDate = datetime.strptime(
                    "-".join([str(year), str(month), str(libCalendar.monthrange(year, month)[1])]), '%Y-%m-%d')

    print(dateList)

    if (userType == "USER"):
        providerID = request.POST.get('classTask')
        provider = Provider.objects.filter(id=providerID).first()
        providerJobType = provider.jobType
        for d in dateList:
            if CurrTime != "":
                Tasks.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=d,
                                    user=current_user, classTask=providerJobType, provider=provider,
                                    carer=current_user.carer, start_time=CurrTime)
            else:
                Tasks.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=d,
                                    user=current_user, classTask=providerJobType, provider=provider,
                                    carer=current_user.carer)
    elif (userType == "CARER"):
        providerID = request.POST.get('classTask')
        provider = Provider.objects.filter(id=providerID).first()
        providerJobType = provider.jobType
        seniorID = request.POST.get('selectSenior')
        senior = User.objects.filter(id=seniorID).first()
        for d in dateList:
            if CurrTime != "":
                Tasks.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=d,
                                    carer=current_user, classTask=providerJobType, provider=provider, user=senior,
                                    start_time=CurrTime)
            else:
                Tasks.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=d,
                                    carer=current_user, classTask=providerJobType, provider=provider, user=senior)
    elif (userType == "PROVIDER"):
        seniorID = request.POST.get('classTask')
        senior = User.objects.filter(id=seniorID).first()
        providerJobType = current_user.jobType
        for d in dateList:
            if CurrTime != "":
                Tasks.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=d,
                                    provider=current_user, classTask=providerJobType, user=senior, start_time=CurrTime)
            else:
                Tasks.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=d,
                                    provider=current_user, classTask=providerJobType, user=senior)

    return HttpResponseRedirect("/getcare/" + current_user.key + "/index/ALL")


def assignNotes(notes):
    carerNote = Notes.objects.filter().first()  # code is bad, if first note is deleted then notes will always display wrong if no note is present
    seniorNote = Notes.objects.filter().first()
    providerNote = Notes.objects.filter().first()
    arrayOfNotes = [0] * 3
    for note in notes:
        if note.carer != None:
            carerNote = note
        elif note.user != None:
            seniorNote = note
        elif note.provider != None:
            providerNote = note
    return carerNote, seniorNote, providerNote


def viewtask(request, currentKey, task_id):
    if request.method == "GET":
        current_user, userType = findingUser(currentKey)
        if current_user:
            current_task = getTask(task_id, currentKey)
            notes = findingNotes(current_task)
            carerNote, seniorNote, providerNote = assignNotes(notes)
            if current_task:
                if userType == "USER":
                    return render(request, 'GetCare/tasks.html',
                                  {'task': current_task, 'user': current_user, 'carerNote': carerNote,
                                   'seniorNote': seniorNote, 'providerNote': providerNote})
                elif userType == "CARER":
                    return render(request, 'GetCare/tasks_carer.html',
                                  {'task': current_task, 'user': current_user, 'carerNote': carerNote,
                                   'seniorNote': seniorNote, 'providerNote': providerNote})
                elif userType == "PROVIDER":
                    return render(request, 'GetCare/tasks_provider.html',
                                  {'task': current_task, 'user': current_user, 'carerNote': carerNote,
                                   'seniorNote': seniorNote, 'providerNote': providerNote})
                else:
                    return render(request, 'GetCare/tasks.html', {'task': current_task, 'user': current_user})
        else:
            return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")

    changeTask = getTask(task_id, currentKey)

    if changeTask:
        current_user, typeUser = findingUser(currentKey)
        if typeUser == "USER":
            User_tasks = Tasks.objects.filter(user=current_user)
        elif typeUser == "CARER":
            User_tasks = Tasks.objects.filter(carer=current_user)
        elif typeUser == "PROVIDER":
            User_tasks = Tasks.objects.filter(provider=current_user)

        changeTask.title = request.POST.get('title')
        changeTask.detail = request.POST.get('detail')
        changeTask.start_date = request.POST.get('date')
        changeTask.comments = request.POST.get('comments')
        if request.POST.get('time') != "":
            changeTask.start_time = request.POST.get('time')
        if request.POST.get('finish') == "finished":
            changeTask.status = 1
        else:
            changeTask.status = 0
        changeTask.save()
        if request.POST.get('delete') == "deleted":
            changeTask.delete()
        if request.POST.get('newnote') != None:
            addNote(currentKey, request.POST.get('newnote'), task_id)
        if request.POST.get('changenote') != None:
            changenote = request.POST.get('changenote')
            changeNote(currentKey, changenote, task_id)
        return HttpResponseRedirect("/getcare/" + str(currentKey) + "/index/ALL")
    else:
        return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")


def info(request, currentKey):
    current_user, userType = findingUser(currentKey)
    if request.method == "GET":
        return render(request, 'GetCare/info.html', {'user': current_user})

    current_user.firstName = request.POST.get('firstName')
    current_user.lastName = request.POST.get('lastName')
    current_user.email = request.POST.get('email')
    current_user.phone = request.POST.get('phone')
    if request.POST.get('password') != "":
        current_user.password = request.POST.get('password')

    if userType == "USER":
        if request.POST.get('carerCode') == "":
            return HttpResponse(r"Please fill in your Carer Relation Code!")
        else:
            try:
                current_user.carer = Carer.objects.get(relation_code=request.POST.get('carerCode'))
            except:
                return HttpResponse(r"Your Carer Relation Code is incorrect!")

        if request.POST.get('careCode') == "":
            return HttpResponse(r"Please fill in your Care Provider Code!")
        else:
            try:
                current_user.careProvider = Provider.objects.get(relation_code=request.POST.get('careCode'))
            except:
                return HttpResponse(r"Your Care Provider Code is incorrect!")

        if request.POST.get('medicationCode') == "":
            return HttpResponse(r"Please fill in your Medication Provider Code!")
        else:
            try:
                current_user.medicationProvider = Provider.objects.get(relation_code=request.POST.get('medicationCode'))
            except:
                return HttpResponse(r"Your Medication Provider Code is incorrect!")

        if request.POST.get('communicationCode') == "":
            return HttpResponse(r"Please fill in your Communication Provider Code!")
        else:
            try:
                current_user.communicationProvider = Provider.objects.get(
                    relation_code=request.POST.get('communicationCode'))
            except:
                return HttpResponse(r"Your Communication Provider Code is incorrect!")

        if request.POST.get('activityCode') == "":
            return HttpResponse(r"Please fill in your Activity Provider Code!")
        else:
            try:
                current_user.activityProvider = Provider.objects.get(relation_code=request.POST.get('activityCode'))
            except:
                return HttpResponse(r"Your Activity Provider Code is incorrect!")

        if request.POST.get('mealCode') == "":
            HttpResponse(r"Please fill in your Meal Provider Code!")
        else:
            try:
                current_user.mealProvider = Provider.objects.get(relation_code=request.POST.get('mealCode'))
            except:
                return HttpResponse(r"Your Meal Provider Code is incorrect!")
    ### CURRENTLY UNREACHABLE CODE
    # current_user.password = request.POST.get('password')
    # if request.POST.get('carerCode') != "":
    #   current_user.carer = Carer.objects.get(relation_code = request.POST.get('carerCode'))
    # if request.POST.get('careCode') != "":
    #    current_user.careProvider = Provider.objects.get(relation_code = request.POST.get('careCode'))
    # if request.POST.get('medicationCode') != "":
    #   current_user.medicationProvider = Provider.objects.get(relation_code = request.POST.get('medicationCode'))
    # if request.POST.get('communicationCode') != "":
    #    current_user.communicationProvider = Provider.objects.get(relation_code = request.POST.get('communicationCode'))
    # if request.POST.get('activityCode') != "":
    #    current_user.activityProvider = Provider.objects.get(relation_code = request.POST.get('activityCode'))
    # if request.POST.get('mealCode') != "":
    #    current_user.mealProvider = Provider.objects.get(relation_code = request.POST.get('mealCode'))
    # if request.POST.get('medicationCode') != whategerbt the value happens to be
    # try:
    # current_user.carer = Carer.objects.get(relation_code = request.POST.get('carerCode'))
    # current_user.careProvider = Provider.objects.get(relation_code = request.POST.get('careCode'))
    # current_user.medicationProvider = Provider.objects.get(relation_code = request.POST.get('medicationCode'))
    # current_user.communicationProvider = Provider.objects.get(relation_code = request.POST.get('communicationCode'))
    # current_user.activityProvider = Provider.objects.get(relation_code = request.POST.get('activityCode'))
    # current_user.mealProvider = Provider.objects.get(relation_code = request.POST.get('mealCode'))
    # except:
    # return HttpResponse(r"Your Relation Code is incorrect!")
    #  current_user.save()
    return HttpResponseRedirect("/getcare/" + str(currentKey) + "/index/ALL")


def newnote(request, currentKey):
    current_user, userType = findingUser(currentKey)
    if request.method == "GET":
        if userType == "USER":
            return render(request, 'GetCare/newnote.html', {'user': current_user})
        elif userType == "CARER":
            return render(request, 'GetCare/newnote_carer.html', {'user': current_user})
        elif userType == "PROVIDER":
            return render(request, 'GetCare/newnote_provider.html', {'user': current_user})
        else:
            return render(request, 'GetCare/newnote.html', {'user': current_user})

    currTitle = request.POST.get('title')
    CurrDate = request.POST.get('date')
    CurrDetail = request.POST.get('detail')
    myProvider = None

    if (userType == "USER"):
        Notes.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=CurrDate,
                             user=current_user, provider=myProvider, carer=current_user.carer)
    elif (userType == "CARER"):
        Notes.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=CurrDate,
                             carer=current_user)
    elif (userType == "PROVIDER"):
        Notes.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=CurrDate,
                             provider=current_user)

    return HttpResponseRedirect("/getcare/" + current_user.key + "/index/ALLnotes")


def viewnote(request, currentKey, note_id):
    if request.method == "GET":
        current_user, userType = findingUser(currentKey)
        if current_user:
            current_note = getNote(note_id, currentKey)
            if current_note:
                if userType == "USER":
                    return render(request, 'GetCare/notes.html', {'note': current_note, 'user': current_user})
                elif userType == "CARER":
                    return render(request, 'GetCare/notes_carer.html', {'note': current_note, 'user': current_user})
                elif userType == "PROVIDER":
                    return render(request, 'GetCare/notes_provider.html', {'note': current_note, 'user': current_user})
                else:
                    return render(request, 'GetCare/notes.html', {'note': current_note, 'user': current_user})
        else:
            return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")

    changeNote = getNote(note_id, currentKey)

    if changeNote:
        current_user, typeUser = findingUser(currentKey)
        if typeUser == "USER":
            User_notes = Notes.objects.filter(user=current_user)
        elif typeUser == "CARER":
            User_notes = Notes.objects.filter(carer=current_user)
        elif typeUser == "PROVIDER":
            User_notes = Notes.objects.filter(provider=current_user)

        changeNote.title = request.POST.get('title')
        changeNote.detail = request.POST.get('detail')
        changeNote.start_date = request.POST.get('date')
        changeNote.start_time = request.POST.get('time')
        # if request.POST.get('finish') == "finished":
        #     changeTask.status = 1
        # else:
        #     changeTask.status = 0
        # changeTask.save()
        if request.POST.get('delete') == "deleted":
            changeNote.delete()

        return HttpResponseRedirect("/getcare/" + str(currentKey) + "/index/ALL")
    else:
        return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")


def results(request, currentKey):
    current_user, userType = findingUser(currentKey)
    if (current_user == False):
        return HttpResponse("Invalid key" + r"<br><a href=\getcare\login> Login </a>")

    User_tasks = findingTasks(current_user, userType)
    result_tasks = []

    if request.method == "GET":
        text = request.GET.get('search').lower()
        print(text)
        tokens = str(text).split(" ")
        print(tokens)

        has_token = -1
        for task in User_tasks:
            taskTitle = task.title.lower()
            for token in tokens:
                if token not in taskTitle:
                    has_token = 0
                    break
            if has_token != 0:
                result_tasks.append(task)
            has_token = -1

    if userType == "CARER":
        return render(request, 'GetCare/results_carer.html', {'user': current_user, 'Tasks': result_tasks})
    elif userType == "PROVIDER":
        return render(request, 'GetCare/results_provider.html', {'user': current_user, 'Tasks': result_tasks})
    elif userType == "USER":
        return render(request, 'GetCare/results.html', {'user': current_user, 'Tasks': result_tasks})
    return render(request, 'GetCare/results.html', {'user': current_user, 'Tasks': result_tasks})


def newTaskForMe(request, currentKey):
    current_user, userType = findingUser(currentKey)

    templates = Template.objects.filter(ownerEmail=current_user.email)
    # filter "new task for me/carer"
    jstemplates = serializers.serialize("json", templates)
    print(jstemplates)

    if request.method == "GET":
        relationsTwo = findSeniors(currentKey)
        return render(request, 'GetCare/newtaskforme.html', {'user': current_user, 'relationsTwo': relationsTwo, 'templates': templates, 'jstemplates': jstemplates})

    taskType = request.POST.get('classTask')
    currTitle = request.POST.get('title')
    CurrDate = request.POST.get('date')
    CurrDetail = request.POST.get('detail')
    CurrTime = request.POST.get('time')
    Frequency = request.POST.get('task-frequency')

    print('time', CurrTime)

    # CurrComment = request.POST.get('comments')
    # CurrTime = request.POST.get('time')
    # myProvider = None

    dateList = []
    
    if Frequency == "Once":
        dateList.append(CurrDate)
    else: # Week or Month
        EndDate = request.POST.get('end-date')
        Starts = datetime.strptime(CurrDate, '%Y-%m-%d')
        Ends = datetime.strptime(EndDate, '%Y-%m-%d')

        if CurrDate >= EndDate:
            return HttpResponse(r"Task End Date should be later than Task Start Date!")

    if Frequency == "Week":
        newDate = Starts
        while newDate <= Ends:
            dateList.append(newDate.strftime('%Y-%m-%d'))
            newDate = newDate + timedelta(weeks=1)
    elif Frequency == "Month":
        day = Starts.day
        month = Starts.month
        year = Starts.year
        newDate = Starts
        while newDate <= Ends:
            print(newDate)
            dateList.append(newDate.strftime('%Y-%m-%d'))
            if month != 12:
                month += 1
            else:
                year += 1
                month = 1
            if libCalendar.monthrange(year, month)[1] >= day:
                newDate = datetime.strptime("-".join([str(year), str(month), str(day)]), '%Y-%m-%d')
            else:
                newDate = datetime.strptime(
                    "-".join([str(year), str(month), str(libCalendar.monthrange(year, month)[1])]), '%Y-%m-%d')

    print(dateList)

    seniorID = request.POST.get('selectSenior')
    senior = User.objects.filter(id=seniorID).first()
    for d in dateList:
        if CurrTime != "":
            Tasks.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=d,
                                    carer=current_user, classTask=taskType, user=senior,start_time=CurrTime)
        else:
            Tasks.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=d,
                                    carer=current_user, classTask=taskType, user=senior)
    return HttpResponseRedirect("/getcare/" + current_user.key + "/index/ALL")    
    
def newTodo(request, currentKey):
    current_user, userType = findingUser(currentKey)

    User_tasks = findingTasks(current_user, userType)
    
    result_tasks = []
    for i in User_tasks:
        if i.status == 0: # add for incompleted tasks
            result_tasks.append(i)

    if request.method == "GET":
        if userType == "USER":
            return render(request, 'GetCare/newtodo.html', {'user': current_user, 'Tasks': result_tasks})
        elif userType == "CARER":
            return render(request, 'GetCare/newtodo_carer.html', {'user': current_user, 'Tasks': result_tasks})
        elif userType == "PROVIDER":
            return render(request, 'GetCare/newtodo_provider.html', {'user': current_user, 'Tasks': result_tasks})
        else:
            return render(request, 'GetCare/newtodo.html', {'user': current_user, 'relations': relations})

    currTitle = request.POST.get('title')
    CurrDate = request.POST.get('date')
    CurrDetail = request.POST.get('detail')
    CurrTime = request.POST.get('time')
    CurrTaskid = request.POST.get('related-task')
    if (CurrTaskid != "" and CurrTaskid != 'satodo'):
        #specify task
        specifiedTask = Tasks.objects.get(id=CurrTaskid)
        print(CurrTaskid)
        print(specifiedTask)
        CurrType = specifiedTask.classTask
    else:
        specifiedTask = None
        CurrType = request.POST.get('todo-type')

    if (userType == "USER"):
        if CurrTime != "":
            Todos.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=CurrDate,
                                user=current_user, classTodo=CurrType, start_time=CurrTime, relatedTask=specifiedTask)
        else:
            Todos.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=CurrDate,
                                user=current_user, classTodo=CurrType, relatedTask=specifiedTask)
    elif (userType == "CARER"):
        if CurrTime != "":
            Todos.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=CurrDate,
                                carer=current_user, classTodo=CurrType, start_time=CurrTime, relatedTask=specifiedTask)
        else:
            Todos.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=CurrDate,
                                carer=current_user, classTodo=CurrType, relatedTask=specifiedTask)
    elif (userType == "PROVIDER"):
            if CurrTime != "":
                Todos.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=CurrDate,
                                    provider=current_user, classTodo=CurrType, start_time=CurrTime, relatedTask=specifiedTask)
            else:
                Todos.objects.create(title=currTitle, create_date=timezone.now(), detail=CurrDetail, start_date=CurrDate,
                                    provider=current_user, classTodo=CurrType, relatedTask=specifiedTask)

    return HttpResponseRedirect("/getcare/" + current_user.key + "/todolist/ALL")
    
def todolist(request, currentKey, classTodo):
    # Finding current user by Key
    current_user, userType = findingUser(currentKey)

    if (current_user == False):
        return HttpResponse("Invalid key" + r"<br><a href=\getcare\login> Login </a>")
    # Loading current user Tasks
    User_todos = findingTodos(current_user, userType)

    # Filting tasks which should be display
    result_todos = []
    if classTodo == "ALL":
        for i in User_todos:
            result_todos.append(i)
    elif classTodo == "INCOMPLETED":
        for i in User_todos:
            if i.status == 0:
                result_todos.append(i)
    elif classTodo == "COMPLETED":
        for i in User_todos:
            if i.status == 1:
                result_todos.append(i)
    else:
        for i in User_todos:
            if i.classTodo == classTodo and i.status == 0:
                # may need revise for classTodo
                result_todos.append(i)

    if userType == "USER":
        return render(request, 'GetCare/todolist.html', {'user': current_user, 'Todos': result_todos})
    elif userType == "CARER":
        return render(request, 'GetCare/todolist_carer.html', {'user': current_user, 'Todos': result_todos})
    elif userType == "PROVIDER":
        return render(request, 'GetCare/todolist_provider.html', {'user': current_user, 'Todos': result_todos})
    else:
        return render(request, 'GetCare/todolist.html', {'user': current_user, 'Todos': result_todos})

def viewtodo(request, currentKey, todo_id):
    if request.method == "GET":
        current_user, userType = findingUser(currentKey)
        if current_user:
            current_todo = Todos.objects.get(id=todo_id)
            # current_todo = getTodo(todo_id, currentKey)
            if current_todo:
                if userType == "USER":
                    return render(request, 'GetCare/todos.html',
                                  {'todo': current_todo, 'user': current_user})
                elif userType == "CARER":
                    return render(request, 'GetCare/todos_carer.html',
                                  {'todo': current_todo, 'user': current_user})
                elif userType == "PROVIDER":
                    return render(request, 'GetCare/todos_provider.html',
                                  {'todo': current_todo, 'user': current_user})
                else:
                    return render(request, 'GetCare/todos.html', {'todo': current_todo, 'user': current_user})
        else:
            return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")

    changeTodo = Todos.objects.get(id=todo_id)

    if changeTodo:
        current_user, typeUser = findingUser(currentKey)
        if typeUser == "USER":
            User_todos = Todos.objects.filter(user=current_user)
        elif typeUser == "CARER":
            User_todos = Todos.objects.filter(carer=current_user)
        elif typeUser == "PROVIDER":
            User_todos = Todos.objects.filter(provider=current_user)

        changeTodo.title = request.POST.get('title')
        changeTodo.detail = request.POST.get('detail')
        changeTodo.start_date = request.POST.get('date')
        if request.POST.get('time') != "":
            changeTodo.start_time = request.POST.get('time')


        if request.POST.get('finish') == "finished":
            changeTodo.status = 1
        else:
            changeTodo.status = 0
        changeTodo.save()

        if request.POST.get('delete') == "deleted":
            changeTodo.delete()

        return HttpResponseRedirect("/getcare/" + str(currentKey) + "/todolist/INCOMPLETED")
    else:
        return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")

def templatelist(request, currentKey, classTemplate):
    # Finding current user by Key
    current_user, userType = findingUser(currentKey)

    if (current_user == False):
        return HttpResponse("Invalid key" + r"<br><a href=\getcare\login> Login </a>")

    # all templates, filter out those carer's own task
    User_templates = Template.objects.filter(ownerEmail=current_user.email, provider__isnull=False)

    # Filting tasks which should be display
    result_templates = []
    if classTemplate == "ALL":
        for i in User_templates:
            result_templates.append(i)
    else:
        for i in User_templates:
            if i.classTask == classTemplate:
                # may need revise for classTodo
                result_templates.append(i)

    if userType == "USER":
        return render(request, 'GetCare/templatelist.html', {'user': current_user, 'Templates': result_templates})
    elif userType == "CARER":
        return render(request, 'GetCare/templatelist_carer.html', {'user': current_user, 'Templates': result_templates})
    elif userType == "PROVIDER":
        return render(request, 'GetCare/templatelist_provider.html', {'user': current_user, 'Templates': result_templates})
    else:
        return render(request, 'GetCare/templatelist.html', {'user': current_user, 'Templates': result_templates})

def newTemplate(request, currentKey):
    current_user, userType = findingUser(currentKey)

    if request.method == "GET":
        if userType == "USER":
            relations = findProviders(currentKey)
            relationsTwo = findSeniors(currentKey)
            print(relationsTwo)
            return render(request, 'GetCare/newtemplate.html',
                          {'user': current_user, 'relations': relations, 'relationsTwo': relationsTwo})
        elif userType == "CARER":
            relations = findProviders(currentKey)
            relationsTwo = findSeniors(currentKey)
            print(relationsTwo)
            return render(request, 'GetCare/newtemplate_carer.html',
                          {'user': current_user, 'relations': relations, 'relationsTwo': relationsTwo})
        elif userType == "PROVIDER":
            relations = findSeniors(currentKey)
            return render(request, 'GetCare/newtemplate_provider.html', {'user': current_user, 'relations': relations})
        else:
            return render(request, 'GetCare/newtemplate.html', {'user': current_user, 'relations': relations})

    currName = request.POST.get('name')
    currTitle = request.POST.get('title')
    CurrDetail = request.POST.get('detail')
    CurrTime = request.POST.get('time')
    Frequency = request.POST.get('task-frequency')

    print('time', CurrTime)

    if Frequency == "Once":
        frequency = 0
    elif Frequency == "Week":
        frequency = 1
    elif Frequency == "Month":
        frequency = 2

    if (userType == "USER"):
        providerID = request.POST.get('classTask')
        provider = Provider.objects.filter(id=providerID).first()
        providerJobType = provider.jobType
        if CurrTime != "":
            Template.objects.create(name=currName, ownerEmail=current_user.email, title=currTitle, detail=CurrDetail,
                                    user=current_user, classTask=providerJobType, provider=provider, frequency=frequency,
                                    carer=current_user.carer, start_time=CurrTime)
        else:
            Template.objects.create(name=currName, ownerEmail=current_user.email, title=currTitle, detail=CurrDetail,
                                    user=current_user, classTask=providerJobType, provider=provider, frequency=frequency,
                                    carer=current_user.carer)
    elif (userType == "CARER"):
        providerID = request.POST.get('classTask')
        provider = Provider.objects.filter(id=providerID).first()
        providerJobType = provider.jobType
        seniorID = request.POST.get('selectSenior')
        senior = User.objects.filter(id=seniorID).first()
        if CurrTime != "":
            Template.objects.create(name=currName, ownerEmail=current_user.email, title=currTitle, detail=CurrDetail,
                                    user=senior, classTask=providerJobType, provider=provider, frequency=frequency,
                                    carer=current_user, start_time=CurrTime)
        else:
            Template.objects.create(name=currName, ownerEmail=current_user.email, title=currTitle, detail=CurrDetail,
                                    user=senior, classTask=providerJobType, provider=provider, frequency=frequency,
                                    carer=current_user)
    elif (userType == "PROVIDER"):
        seniorID = request.POST.get('classTask')
        senior = User.objects.filter(id=seniorID).first()
        providerJobType = current_user.jobType
        if CurrTime != "":
            Template.objects.create(name=currName, ownerEmail=current_user.email, title=currTitle, detail=CurrDetail,
                                    user=senior, classTask=providerJobType, provider=current_user, frequency=frequency,
                                    carer=senior.carer, start_time=CurrTime)
        else:
            Template.objects.create(name=currName, ownerEmail=current_user.email, title=currTitle, detail=CurrDetail,
                                    user=senior, classTask=providerJobType, provider=current_user, frequency=frequency,
                                    carer=senior.carer)

    return HttpResponseRedirect("/getcare/" + current_user.key + "/templatelist/ALL")

def viewtemplate(request, currentKey, template_id):
    if request.method == "GET":
        current_user, userType = findingUser(currentKey)
        if current_user:
            current_template = Template.objects.get(id=template_id)
            if current_template:
                if userType == "USER":
                    relations = findProviders(currentKey)
                    return render(request, 'GetCare/viewtemplate.html',
                                  {'template': current_template, 'user': current_user, 'relations': relations})
                elif userType == "CARER":
                    relations = findProviders(currentKey)
                    relationsTwo = findSeniors(currentKey)
                    return render(request, 'GetCare/viewtemplate_carer.html',
                                  {'template': current_template, 'user': current_user, 'relations': relations, 'relationsTwo': relationsTwo})
                elif userType == "PROVIDER":
                    relations = findSeniors(currentKey)
                    return render(request, 'GetCare/viewtemplate_provider.html',
                                  {'template': current_template, 'user': current_user, 'relations': relations})
                else:
                    return render(request, 'GetCare/viewtemplate.html', {'template': current_template, 'user': current_user})
        else:
            return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")

    changeTemplate = Template.objects.get(id=template_id)

    freMap = {'Once':0, 'Week':1, 'Month':2}
    if changeTemplate:
        current_user, typeUser = findingUser(currentKey)
        # if typeUser == "USER":
        #     User_todos = Template.objects.filter(user=current_user)
        # elif typeUser == "CARER":
        #     User_todos = Todos.objects.filter(carer=current_user)
        # elif typeUser == "PROVIDER":
        #     User_todos = Todos.objects.filter(provider=current_user)

        changeTemplate.name = request.POST.get('name')
        changeTemplate.title = request.POST.get('title')
        changeTemplate.detail = request.POST.get('detail')
        if request.POST.get('time') != '':
            changeTemplate.start_time = request.POST.get('time')
        changeTemplate.provider = Provider.objects.filter(id=request.POST.get('classTask')).first()

        changeTemplate.frequency = freMap[request.POST.get('task-frequency')]

        if request.POST.get('button') == "delete":
            changeTemplate.delete()
        if request.POST.get('button') == "submit":
            changeTemplate.save()

        return HttpResponseRedirect("/getcare/" + str(currentKey) + "/templatelist/ALL")
    else:
        return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")

def templatelistforme(request, currentKey, classTemplate):
    # Finding current user by Key
    current_user, userType = findingUser(currentKey)

    if (current_user == False):
        return HttpResponse("Invalid key" + r"<br><a href=\getcare\login> Login </a>")
    if (userType != "CARER"):
        return HttpResponse("Wrong Page")

    # all templates
    User_templates = Template.objects.filter(ownerEmail=current_user.email, provider__isnull=True)

    # Filting tasks which should be display
    result_templates = []
    if classTemplate == "ALL":
        for i in User_templates:
            result_templates.append(i)
    else:
        for i in User_templates:
            if i.classTask == classTemplate:
                # may need revise for classTodo
                result_templates.append(i)

    return render(request, 'GetCare/templatelistforme.html', {'user': current_user, 'Templates': result_templates})


def viewtemplateforme(request, currentKey, template_id):
    if request.method == "GET":
        current_user, userType = findingUser(currentKey)
        if current_user:
            current_template = Template.objects.get(id=template_id)
            if current_template:
                if userType == "CARER":
                    relations = findProviders(currentKey)
                    relationsTwo = findSeniors(currentKey)
                    return render(request, 'GetCare/viewtemplateforme.html',
                                  {'template': current_template, 'user': current_user, 'relations': relations, 'relationsTwo': relationsTwo})
        else:
            return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")

    changeTemplate = Template.objects.get(id=template_id)

    freMap = {'Once':0, 'Week':1, 'Month':2}
    if changeTemplate:
        current_user, typeUser = findingUser(currentKey)

        changeTemplate.name = request.POST.get('name')
        changeTemplate.title = request.POST.get('title')
        changeTemplate.detail = request.POST.get('detail')
        if request.POST.get('time') != '':
            changeTemplate.start_time = request.POST.get('time')
        changeTemplate.classTask = request.POST.get('classTask')
        changeTemplate.frequency = freMap[request.POST.get('task-frequency')]

        if request.POST.get('button') == "delete":
            changeTemplate.delete()
        if request.POST.get('button') == "submit":
            changeTemplate.save()

        return HttpResponseRedirect("/getcare/" + str(currentKey) + "/templatelistforme/ALL")
    else:
        return HttpResponse(r"User information error.<br><a href=\getcare\login> Try again </a>")

def newTemplateForMe(request, currentKey):
    current_user, userType = findingUser(currentKey)

    if request.method == "GET":
        if userType == "CARER":
            relations = findProviders(currentKey)
            relationsTwo = findSeniors(currentKey)
            print(relationsTwo)
            return render(request, 'GetCare/newtemplateforme.html',
                          {'user': current_user, 'relations': relations, 'relationsTwo': relationsTwo})

    currName = request.POST.get('name')
    currTitle = request.POST.get('title')
    CurrDetail = request.POST.get('detail')
    CurrTime = request.POST.get('time')
    Frequency = request.POST.get('task-frequency')
    taskType = request.POST.get('classTask')

    print('time', CurrTime)

    if Frequency == "Once":
        frequency = 0
    elif Frequency == "Week":
        frequency = 1
    elif Frequency == "Month":
        frequency = 2

    if (userType == "CARER"):
        seniorID = request.POST.get('selectSenior')
        senior = User.objects.filter(id=seniorID).first()
        if CurrTime != "":
            Template.objects.create(name=currName, ownerEmail=current_user.email, title=currTitle, detail=CurrDetail,
                                    user=senior, classTask=taskType, frequency=frequency,
                                    carer=current_user, start_time=CurrTime)
        else:
            Template.objects.create(name=currName, ownerEmail=current_user.email, title=currTitle, detail=CurrDetail,
                                    user=senior, classTask=taskType, frequency=frequency,
                                    carer=current_user)

    return HttpResponseRedirect("/getcare/" + current_user.key + "/templatelistforme/ALL")