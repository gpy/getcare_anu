# Generated by Django 3.1 on 2020-10-06 04:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('getcareapp', '0009_auto_20201006_1435'),
    ]

    operations = [
        migrations.AddField(
            model_name='tasks',
            name='provider',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.provider'),
        ),
    ]
