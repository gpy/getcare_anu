# Generated by Django 3.1.7 on 2021-04-29 09:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('getcareapp', '0015_auto_20210428_1545'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='relation_code',
            field=models.CharField(default='1234567890', max_length=10),
        ),
    ]
