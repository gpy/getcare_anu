# Generated by Django 3.1.7 on 2021-05-10 16:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('getcareapp', '0017_auto_20210504_1152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carerseniorrelationship',
            name='carer',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.carer'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='carerseniorrelationship',
            name='senior',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.user'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='providercarerrelationship',
            name='carer',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.carer'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='providercarerrelationship',
            name='provider',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.provider'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='providerseniorrelationship',
            name='provider',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.provider'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='providerseniorrelationship',
            name='senior',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.user'),
            preserve_default=False,
        ),
    ]
