# Generated by Django 3.2 on 2021-04-21 03:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('getcareapp', '0013_notes'),
    ]

    operations = [
        migrations.AddField(
            model_name='carer',
            name='activityProvider',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.provider'),
        ),
        migrations.AddField(
            model_name='carer',
            name='careProvider',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.provider'),
        ),
        migrations.AddField(
            model_name='carer',
            name='communicationProvider',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.provider'),
        ),
        migrations.AddField(
            model_name='carer',
            name='mealProvider',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.provider'),
        ),
        migrations.AddField(
            model_name='carer',
            name='medicationProvider',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.provider'),
        ),
        migrations.CreateModel(
            name='ProviderSeniorRelationship',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_date', models.DateField()),
                ('provider', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.provider')),
                ('senior', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.user')),
            ],
        ),
        migrations.CreateModel(
            name='ProviderCarerRelationship',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_date', models.DateField()),
                ('carer', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.carer')),
                ('provider', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='getcareapp.provider')),
            ],
        ),
    ]
