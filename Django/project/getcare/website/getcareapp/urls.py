from django.urls import path

from . import views

app_name = 'getcare'

urlpatterns = [
    path('',views.login, name='login'),  
    path('register/',views.register, name='register'),
    path('login/',views.login, name='login'),
    path('<str:currentKey>/index/<str:classTask>', views.index, name='index'),
    path('<str:currentKey>/newtask/', views.newTask, name='newtask'),
    path('<str:currentKey>/viewtask/<int:task_id>', views.viewtask, name='viewtask'),
    path('<str:currentKey>/info/', views.info, name='info'),
    path('<str:currentKey>/viewnote/<int:note_id>',views.viewnote, name='viewnote'),
    path('<str:currentKey>/newnote/', views.newnote, name='newnote'),
    path('<str:currentKey>/calendar/<str:classType>', views.calendar, name='calendar'),
    path('<str:currentKey>/uploadcsv/', views.uploadCSV, name='uploadcsv'),

    path('<str:currentKey>/viewrelation/', views.viewRelations, name='viewRelations'),
    path('<str:currentKey>/viewrelation_carer/', views.viewRelations, name='viewRelationsCarer'),
    path('<str:currentKey>/viewrelation_provider/', views.viewRelations, name='viewRelationsProviders'),

    path('<str:currentKey>/viewrelation/', views.addRelation, name='addRelation'),
    path('<str:currentKey>/viewrelation/', views.removeRelation, name='removeRelation'),

    path('<str:currentKey>/viewtask/<int:task_id>', views.addNote, name='addnote'),

    path('<str:currentKey>/results/', views.results, name='results'),
    path('<str:currentKey>/newtaskforme/', views.newTaskForMe, name='newtaskforme'),
    path('<str:currentKey>/newtodo/', views.newTodo, name='newtodo'),
    path('<str:currentKey>/todolist/<str:classTodo>', views.todolist, name='todolist'),
    path('<str:currentKey>/viewtodo/<int:todo_id>', views.viewtodo, name='viewtodo'),

    path('<str:currentKey>/templatelist/<str:classTemplate>', views.templatelist, name='templatelist'),
    path('<str:currentKey>/newtemplate/', views.newTemplate, name='newtemplate'),
    path('<str:currentKey>/viewtemplate/<int:template_id>', views.viewtemplate, name='viewtemplate'),

    path('<str:currentKey>/templatelistforme/<str:classTemplate>', views.templatelistforme, name='templatelistforme'),
    path('<str:currentKey>/newtemplateforme/', views.newTemplateForMe, name='newtemplateforme'),
    path('<str:currentKey>/viewtemplateforme/<int:template_id>', views.viewtemplateforme, name='viewtemplateforme'),





]