from django.contrib import admin
from.models import *
# Register your models here.

admin.site.register(User)
admin.site.register(Tasks)
admin.site.register(Carer)
admin.site.register(Provider)

admin.site.register(Notes)

admin.site.register(ProviderCarerRelationship)
admin.site.register(ProviderSeniorRelationship)
admin.site.register(CarerSeniorRelationship)

admin.site.register(Todos)
admin.site.register(Template)