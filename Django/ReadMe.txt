1. Please install Python 3.8.3 (Latest) first.

2. Then run the “getcare_anu\Django\GetCare\Scripts\Activate" to activate the Virtual environment of Python: $GetCare\Scripts\Activate

3. Install Django 3.1 (Latest) under the Virtual Environment: $pip install Django

4. Run the Server: python GetCare\Project\website\manage.py runserver



Another way to run GetCare
1. install anaconda on your computer
   according to your operating system, download anaconda from https://www.anaconda.com/products/individual
   install it for your computer
2. install pycharm or vs code on your computer, my advice is pycharm
   according to your operating system, download pycharm from https://www.jetbrains.com/pycharm/download/
   install it for your computer
3. download the project from gitlab
   star your pycharm get project from gitVCS, enter the project github address
   https://gitlab.com/gpy/getcare_anu.git
4. sitting a virtual environment for your project
   form Preference find Python Interpreter, add anaconda as your Python Interpreter.
   then using conda to add django in your virtual env
5. open Terminal and find manage.py in your project
   python manage.py runserver

The biggest advantage of this is that Conda creates a virtual runtime environment for you, and it is more
convenient to install various packages. Don't worry about various system configuration issues, all environment
settings and package installation can be simply completed. And the system will not leave any configuration
files after deleting the project.

