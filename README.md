# What is Getcare?
Getcare is a service which aims to facilitate the caretaking of elderly people by caretakers. It will achieve this by providing the means to communicate and coordinate the efforts of carers trying to care for their elderly loved ones. 

The service will allow a Carer to have visibility of all services and support being provided to the person being cared for in a calendar and schedule view (with ability to change appointments or request particular services for scheduled visits). Information presented will be linked directly to the source allowing dynamic updating (eg. the schedule of visits approved by an in-home support provider or weekly social activities the person being cared for is enrolled in). The Carer, or person being cared for, will be able to add personal activities to the calendar (eg doctor visits). Providers interacting with the person being cared for will be able to provide updates through the app (eg. completion of tasks assigned to an activity, ad-hoc notes, requests to the Carer).

Previous teams working on this project have already developed a prototype web application using Django. This semester’s team will focus on improving the prototype and developing support for mobile users, as well as sourcing feedback from user testers to improve the project later in the semester.



# Getcare Stakeholder Roles and Benefits

 #### Project costs and/or reimbursable expenses
- Cost: None
- Payment directions: payable to University

 #### Client (Stephen Blackburn)
- The client will interact with members of the development team their requirements for the project. At weekly meetings they will assess the state of the project and be given an oppourtunity to voice their suggestions and concerns with the project. At the conclusion of the semester they will have a working, user tested website with server hosting and mobile support.

 #### Members of the Project Development Team
- Members of the development team will attend weekly meetings and client meetings in which the state of the project and future tasks to be completed will be discussed. They will also attend workshops to listen to feedback from both tutors/mentors and members of the shadow team. Development team members will gain the experience of working in a team and in working to build an app and maintain a project.

 #### Techlauncher Assessment Team (Tutors/Mentors)
- The mentors will provide feedback about the current state of the project to the development team in the form of weekly workshops and assessment feedback (audits, etc).

 #### Shadow Team (Ability News)
- The shadow team will provide verbal critique and suggestions throughout the semester during audits and weekly workshops. This will provide the development team with a different perspective and allow us to alter our ways of work to ensure a better result. The Shadow Team will also gain insight into our methods of work and will be able to learn from our experience in what are efficient and inefficient methods of working.

 #### User Testers
- When the project is in a test-ready state toward the end of the semester there may be an oppourtunity for selected users to act as beta testers. Users would be walked through the app and website by the development team, before providing feedback. This would allow us to create a 
list of functionalities that need improving in order to better meet user requirements. User testers would in turn receive a final product that will better suit their needs and be more user friendly for them.

 #### Resources
- The project will be based on a Web application. Technologies include but are not limited to: Python, React, JavaScript, Spring Boot, MongoDB and Git.
- The Project will enable students to develop their Project management, Communication, problem decision-making, professional skills and practice. Students will work on a Project for industry and the tasks undertaken will be assessed for academic credit as outlined in more detail below. The curriculum, that is the content and learning processes, will be defined by the work, and Projects will be offered (as far as possible, and when available) in areas that match the aims and content of the ANU TechLauncher programs.

# Members
| Member           | UID          | Role                               | Spokesperson       |
| ---------------  | -------------|------------------------------------|--------------------|
| Lachlan Barnes   | u6669624     | Cloud Deployment (Project Manager) | Yes                |
| Shimeng Ding     | u6213716     | Front-End Developer                | Yes                |
| Min Zhu          | u6642413     | Front-End Developer                | No                 |
| Wanqi Liao       | u6976221     | Back-End Developer                 | No                 |
| Chen Chen        | u7170032     | Back-End Developer                 | No                 |

- Note that all members can be contacted via their ANU email
# Links
Trello: https://trello.com/b/9DBAkTek/getcare-anu-s1-2021

Google Drive:  https://drive.google.com/drive/folders/1XqRy1V349n2ksqNTzI_m8230h4gwziKI 

Hosted: getcareapp.com

# S2(2021) Documents 


 - [Statement of Work](https://docs.google.com/document/d/1wyJoL0de_OfeiHdEAIdsHU2uPosfsDkiKh3V_rHN-xk/edit)
 - [Industry Project Student Agreement](https://docs.google.com/document/d/1LyY3OLr3oJNzVhwvGLbE0ASI6xb_pu3_/edit)
 - [Decision making log](https://docs.google.com/spreadsheets/d/13ZRlho_WtAuqCAPyS8ZsQjqgtHSn7jjYvF-Yhvdg2iU/edit#gid=0)
 - [Feedback log](https://docs.google.com/spreadsheets/d/1GzvW_FpeWBFo7k-d8rh5UwUvQri1HXmj/edit#gid=975687920)


# S2(2021) Weekly Agenda


**Week 2**
 - [Agenda Summary](https://docs.google.com/document/d/1KuJh9jPOAmSbGD4ljW3tc8HfPM_sq8u0741fXw17-pc/edit)
 - [Team Meeting](https://docs.google.com/document/d/1fJMnBYmtaxY9PzgW2wAk7zvEBEJzuGZRa7GNyKDMnec/edit)
 - [Client Meeting](https://docs.google.com/document/d/1Jr7UfzyktMdg444JGQ1-PyAUbLf_px8RwOCzALGSqgQ/edit)


**Week 3**
 - [Agenda Summary](https://docs.google.com/document/d/1tjiG3Ynyq2dTH8lqcWowXOSk6sIToKUYgul0ptR5DsA/edit)
 - [Team Meeting](https://docs.google.com/document/d/1-tSh_aNoGut3_XbT9hLl3HjAPlg17yVdkw213mf6umM/edit)
 - [Client Meeting](https://docs.google.com/document/d/1FRQPkHEIM00ubouWOK8VNJ7I2Zs4qVvf9vPmaJJNq0E/edit)


**Week 4**
 - [Agenda Summary](https://docs.google.com/document/d/1mBqONlvqBWNK0kSjwf7q7PaiK7O0pOp0JOpfVFgJ37E/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1Os4mAcqQFgrt0fQqT-kaQyAeeyp8C1tXdPi6ULMLypw/edit?usp=sharing)
 - [Client Meeting](https://docs.google.com/document/d/1xn7o7eK5-JF29sKfDH8TkRePk2VSfhVPU1s86GzOcOQ/edit)

**Week 5**
 - [Agenda Summary](https://docs.google.com/document/d/1An41OZYe0hQ7m-OlyL1sfj2aU4QpMk9PLbnsLr9iBNs/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1EXT9Qv6e41TAx5toFpaKzHn5s2b8uQqozWN1qYVsS6I/edit?usp=sharing)
 - [Client Meeting](https://docs.google.com/document/d/1LEvIXIL6t7Pp09xtxRTSmFohgC8Di-A1YwdEGvPM_kI/edit?usp=sharing)

 **Week 6**
 - [Agenda Summary](https://docs.google.com/document/d/1Xq8tH-1w885zVk8LztRBEet6wVsmBfVPOfw4Xbxb67E/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1IiYcYvOOxycaMgopnOyieoniGnnC0bM2RX6gKG19Rtw/edit?usp=sharing)
 - [Client Meeting](https://docs.google.com/document/d/1egxRD9j8o9UhQY0JGKqjFqX423k01g5ZhjjnljOgxbs/edit?usp=sharing)

**Week 7**
 - [Agenda Summary](https://docs.google.com/document/d/11sgHYh_ZiCnXvroro_zc-x0sQd6H-h37jmb4uuZEfcY/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1_WmWseEFEOf02wSk-ZSfF3lZTITAoRB6h5c_K2Bmm-Y/edit?usp=sharing)
 - [Client Meeting](https://docs.google.com/document/d/1p6Jrk_-nq-gNC2Uqb6Lczdag8af2AWeyqiBVCFQINBs/edit?usp=sharing)

**Week 8**
 - [Team Meeting](https://docs.google.com/document/d/18D4hZyaKmZO3NYfy8rsSw454rVrrLgnMEjAuT8h8hGM/edit?usp=sharing)
 - [Client Meeting](https://docs.google.com/document/d/156wzaXav9fRE0gLGDPsBL0h_T6A1Edf-sRX6YMBEbZo/edit?usp=sharing)

**Week 9**
 - [Agenda Summary](https://docs.google.com/document/d/1Ad98NN6Vk-tjY4uFyM_5EtH7vhN8od_9tVfbZnubx6c/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1IidrG3Jn1Gb4lLCxGgP3VPZJRZ1rczGtIkfIF4-9eOY/edit?usp=sharing)

**Week 10**
 - [Agenda Summary](https://docs.google.com/document/d/1-ct4NhT-z5yiJSKh317HWuVJlv8uLeUa2XUxd0f6h6U/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1vbIzF1egTA0Cb6wbABVLFt8hrvwG7ZOtBq8gRAOkZbQ/edit?usp=sharing)
 - [Client Meeting](https://docs.google.com/document/d/1XufHG61L971BRcA4A7uFD1ySk4B-WckL-vqiagPyVbw/edit?usp=sharing)


# S1(2021) Documents 


 - [Statement of Work](https://drive.google.com/file/d/17sh89tHYyWrHP6cHPuDb4RUBTn4Uvp3X/view?usp=sharing)
 - [Industry Project Student Agreement](https://drive.google.com/file/d/1_pJ4iLZwHMcrLM-3JOA-Frg29FPAzCeP/view?usp=sharing)
 - [User Story Map](https://drive.google.com/file/d/16s_7RLuj802QFRPTvEj9EoJMnH1AZX1Q/view?usp=sharing)
 - [Decision making log](https://docs.google.com/spreadsheets/d/1WCXL1EnwVKquGYUWLYEjk-brlaRDXRSc/edit#gid=931183004)
 - [Feedback log](https://drive.google.com/file/d/1WQFSyoArWcgU_TSg9sebSKI0ldBauOEf/view?usp=sharing)


# S1(2021) Weekly Agenda


**Week 2**
 - [Team Meeting](https://docs.google.com/document/d/1-q7HU_2WsbA7UvL6YN0wv57fGSLnKqcnA-DADq3wOOM/edit)
 - [Decision Making](https://docs.google.com/document/d/11Wh3bh_uoyKtcY2QifvXzyKWiyVR4h0-A8DoCH7Hr68/edit)


**Week 3**
 - [Agenda Summary](https://docs.google.com/document/d/1WJCyHD771leOKCRwvRUSQtIn8-7vURQYfkKQFN6Jkgo/edit)
 - [Team Meeting](https://docs.google.com/document/d/1bAE0qaK2qK7_HghpUKBdNBXbgEH1HwpyxWXN5nopyiY/edit)
 - [Client Meeting](https://docs.google.com/document/d/1Nbz3xwmTViTt_BmVU4el_H-ZsmEvk1rbWQKVi15vhsw/edit)
 - [Decision Making](https://docs.google.com/document/d/1N-D1HqJvNbrkZf7fAhfGCTOXKDAIqdUNwv0Yqoc-MG8/edit)


 **Week 4** 
 - [Agenda Summary](https://docs.google.com/document/d/1uZtP0nJ2E7Kdg0_rHQCwTsDJGB-0DnkaPgPOoN2tt_U/edit)
 - [Team Meeting](https://docs.google.com/document/d/1JJzsfqusm0VK-d46O3jPZ08HKqUf7VUT2VbpXNqv424/edit)
 - [Client Meeting](https://docs.google.com/document/d/1dTbRmzas5T9I2BAzlw2j6w0DzYy8KTUqTZolQlf7xJ8/edit?usp=sharing)
 - [Decision Making](https://docs.google.com/document/d/1P6pmUmeVB2UC4QlWHiKnarIwcJqeRw-SqG_oa-8N_0k/edit)


  **Week 5** 
 - [Agenda Summary](https://docs.google.com/document/d/1fN7RYlIf13A_VQug-0J1LbRWXwvUUi1jIFj3NHUuEPk/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1dCIE5L_xuBG-p9kKfzS3eJ-rAtVKDefKZR0QfJTvfxs/edit?usp=sharing)
 - [Client Meeting](https://docs.google.com/document/d/1z-WSuTgf4GnN63pxO-ZfIxjkfbK_Sv-45OuX7QC473s/edit?usp=sharing)
 - [Decision Making](https://docs.google.com/document/d/1REqnTg-q5HBOwz0N2_lEs_k9fWEYCBqkQJ5YekK3kjg/edit)
 

  **Week 6** 
 - [Agenda Summary](https://docs.google.com/document/d/1jhfeUssTGluxKFSLTRrWrclHbmRWz8KqZfmFmVuxjGc/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1ZZs37J45sapGE5GoEs5tdopSY2bIblURtvQcqHZruyI/edit)
 - [Client Meeting](https://docs.google.com/document/d/1lUgGyo_nfRLWfJu5lb4w4OTSezWaST1Ry-yI-knFgYs/edit)
 - [Decision Making](https://docs.google.com/document/d/1HZmMmcBk7gNxYw8JC3kWsrX1CGpWjCeQBRpMXNzLHMU/edit)

 
  **Week 7** 
 - [Agenda Summary](https://docs.google.com/document/d/1mgexvNM2JNJABtSqeWIJkqT1GHTJTOniXXhiySnPlsk/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1Pj1rilQDNMuZo5SMlXfk9Maa95aq_JWjNo_shQ_VNho/edit)
 - [Client Meeting](https://docs.google.com/document/d/1O3zkGFqOFgn6a7uk3jQkV4EyAf1d3_BeFIzFnOYoiRA/edit)
 - [Decision Making](https://docs.google.com/document/d/1cOYI0-q6iHi6zeQeMDou3EF_80s339Whzay5GOkgy7c/edit)


 **Week 8** 
 - [Team Meeting](https://docs.google.com/document/d/1ruc2vPrJF-_fZl48ve9BSXmW3ML3sM5EN7oH5Oajbe4/edit)
 - [Client Meeting -- User Testing](https://docs.google.com/document/d/1Qod8hOjvvTTP3kwlUBJKX67TXwggMw3HQKmCdYd94_I/edit)
 - [Decision Making](https://docs.google.com/document/d/18WBN7TfJmxnnY9Sk_L8K6YQYQsdJdHTsCTbk1Cb8LB4/edit)


 **Week 9** 
 - [Agenda Summary](https://docs.google.com/document/d/1yZhCDFVEZueonb_Q2gcMiE5JiqP0_s3yTpF6mIqb1dU/edit)
 - [Team Meeting](https://docs.google.com/document/d/1oQnVyVqMan629o_nk9IieO6SzxzKPhuKyjw362sRAwg/edit)
 - [Client Meeting](https://docs.google.com/document/d/1R08S0u4Ccn6ICvUCWdlxF-iW5Mfln5UHo2MNolF8AaA/edit)
 - [Decision Making](https://docs.google.com/document/d/1YIhlE__SAifAaVeeScij_gD2YcV3L1SZDNVxzr3eEBQ/edit)


 **Week 10** 
 - [Agenda Summary](https://docs.google.com/document/d/1YVzOXilQ9tpsX5HqZfTRz9Av1J8sLNZidgvWmadSHqg/edit?usp=sharing)
 - [Team Meeting](https://docs.google.com/document/d/1WHhpArIPO_0NWkf5awEAKBRUQqIiupc1eVwdf9gF9zw/edit)
 - [Decision Making](https://docs.google.com/document/d/1jwQRjdLBwys2W9MKe5cmzgz7_YZjBNnMSKeLXxbI4-A/edit)


# S2(2020) Documents 


 - [Statement of Work](https://drive.google.com/file/d/1UpG1HmydJ492avFITxcl_zcebtYLdo_A/view?usp=sharing)
 - [Industry Project Student Agreement](https://drive.google.com/open?id=170t72dfRX-ydoAIMNuvRZ1KOSeNwSO9d)
 - [User Story Map](https://drive.google.com/file/d/16s_7RLuj802QFRPTvEj9EoJMnH1AZX1Q/view?usp=sharing)
 - [Decision making log](https://drive.google.com/file/d/1ZP2ZdNC2I2raFoeStcA9VeZ3XyWBETu1/view?usp=sharing)
 - [Feedback log](https://drive.google.com/file/d/18Unm4KUAD3_anChyPOysh94pv7Mslcyq/view?usp=sharing)
 - [Project schedule & sprint deliveries](https://drive.google.com/file/d/1i_PmIkrxYiXlbtlQX1iglfKYq_1XrbGY/view?usp=sharing)

# S2 (2020) Weekly Agenda
**Week 2**
 - [Team Meetings](https://drive.google.com/file/d/1j5g_mo9ZDaQxa-qM52xCAjuWz1ldKTsr/view?usp=sharing)
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Client Meetings](https://drive.google.com/file/d/1Bw9VZgjLSGwa9jRtian3PS5yjLcXOaTK/view?usp=sharing)

**Week 3**
 - [Team Meetings](https://drive.google.com/file/d/1my1GFxG6p9MdQsWNbgoTiMrDaUUtCMX1/view?usp=sharing)
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)

**Week 4**
 - [Team Meetings](https://drive.google.com/file/d/1xqRwnE87FT3HpsLQQt73IaO9S8Gb5hge/view?usp=sharing)
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Agenda](https://drive.google.com/file/d/1JiVlyS09RwbJMLuQcPB5p_Tamq-XsSQO/view?usp=sharing)

**Week 5**
 - [Team Meetings](https://drive.google.com/file/d/1TXyO2qtQ6mVPptK_2ZF0yXI8mDcteg2r/view?usp=sharing)
 - [Client Meetings](https://drive.google.com/file/d/1VXkXQ4NwPHVyeC3VGMV1dVPSBRCiKK0t/view?usp=sharing)
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Agenda](https://drive.google.com/file/d/1JiVlyS09RwbJMLuQcPB5p_Tamq-XsSQO/view?usp=sharing)
 
**Week 6**
 - [Team Meetings](https://drive.google.com/file/d/1GK2iUWHWCL4rE8YMdrxuoQ2mti82p6bG/view?usp=sharing)
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 
**Week 7**
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)

**Week 8**
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Client Meetings](https://drive.google.com/file/d/1uHtJukJ5c1ka_BVYp2GHKrNAattJx42c/view?usp=sharing)
 - [Team Meetings](https://drive.google.com/file/d/1Cicy0KxIpe6UaZlX6PHvgnNBCPd8IHdK/view?usp=sharing)


**Week 9**
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Team Meetings](https://drive.google.com/file/d/1lTp6568N6GF3fPHitCUMTUb4Ol3RWpSb/view?usp=sharing)
 - [Client Meetings](https://drive.google.com/file/d/1kvW1XeFXe4eU9L3m9C8T4iqZYnkEZam6/view?usp=sharing)



**Week 10**
 - [Decision Making](https://docs.google.com/spreadsheets/d/1a6iektYKlq4noZgkFNZqE1DHMvALZis9bkjkmfzA0tc/edit?usp=sharing)
 - [Team Meetings](https://drive.google.com/file/d/1TP1k88F1NCTAQAWETgyQtNZG5Vd36IbQ/view?usp=sharing)





